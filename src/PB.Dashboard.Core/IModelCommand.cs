﻿namespace PB.Dashboard.Core
{
    public interface IModelCommand<in TInput, out TResult>
    {
        TResult Execute(TInput model);
    }
}