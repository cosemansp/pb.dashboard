﻿using System;

namespace PB.Dashboard.Core
{
    public static class DateTimeExtensions
    {
        public static Int64 ToJsonDate(this DateTime dt)
        {
            var d1 = new DateTime(1970, 1, 1);
            DateTime d2 = dt.ToUniversalTime();
            var ts = new TimeSpan(d2.Ticks - d1.Ticks);
            return (Int64)ts.TotalMilliseconds;
        }

        public static DateTime ToDateTime(this Int64 jsonDate) {
            DateTime dt_1970 = new DateTime(1970, 1, 1);
            long tricks_1970 = dt_1970.Ticks;
            long time_tricks = tricks_1970 + jsonDate * 10000;
            return new DateTime(time_tricks);
        }
    }
}