﻿namespace PB.Dashboard.Core
{
    /// <summary>
    /// Interface for IMapper
    /// </summary>
    /// <typeparam name="TSource">The type of the input.</typeparam>
    /// <typeparam name="TDestination">The type of the output.</typeparam>
    public interface IMapper<in TSource, TDestination>
    {
        /// <summary>
        /// Maps from TSource to TDestination
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The mapped result</returns>
        TDestination Map(TSource source);

        /// <summary>
        /// Maps from TSource to TDestination where you must provide the destination to fill in.
        /// Typically used with collections
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="dest">The destination.</param>
        /// <returns>The mapped result</returns>
        TDestination Map(TSource source, TDestination dest);
    }
}