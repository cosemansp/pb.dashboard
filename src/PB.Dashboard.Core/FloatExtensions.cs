﻿using System;

namespace PB.Dashboard.Core
{
    public static class FloatExtensions
    {
        public static float RoundTo(this float value, float roundto)
        {
            if (Math.Abs(roundto) <= 0)
            {
                return value;
            }
            return (float)Math.Floor(value / roundto) * roundto;
        }
    }
}