﻿using System.Configuration;
using Autofac;
using Autofac.Integration.WebApi;
using PB.Dashboard.Api.Mappers;
using PB.Dashboard.Api.Resources;
using PB.Dashboard.Core;
using PB.Dashboard.Data;
using PB.Dashboard.Domain.Entities;
using PB.Dashboard.Domain.Services;
using Raven.Client;

namespace PB.Dashboard
{
    public class Bootstrapper
    {
        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(typeof(Measurement).Assembly);

            // Mappers
            builder.RegisterType<Measurement2MeasurementResourceMapper>().As<IMapper<Measurement, MeasurementResource>>();
            builder.RegisterType<User2UserResourceMapper>().As<IMapper<User, UserResource>>();
            builder.RegisterType<SpeciesInventory2SpeciesInventoryResourceMapper>().As<IMapper<SpeciesInventory, SpeciesInventoryResource>>();

            // RavenDB
            var docStore = InitializeDocStore();
            builder.RegisterInstance(docStore).As<IDocumentStore>();
            builder.Register(ctx => ctx.Resolve<IDocumentStore>().OpenSession())
                .As<IDocumentSession>()
                .InstancePerLifetimeScope();

            // Other
            builder.RegisterType<AuthService>().As<IAuthService>();
            builder.RegisterType<Auth0Agent>().As<IAuth0Agent>()
                .WithParameter("clientId", ConfigurationManager.AppSettings["auth0:ClientId"])
                .WithParameter("clientSecret", ConfigurationManager.AppSettings["auth0:ClientSecret"])
                .WithParameter("domain", ConfigurationManager.AppSettings["auth0:Domain"])
                .SingleInstance();
            builder.RegisterType<LocalCache>().As<ICache>();

            return builder.Build();
        }

        private static IDocumentStore InitializeDocStore()
        {
            var docstore = new Raven.Client.Document.DocumentStore
            {
                ConnectionStringName = "RavenDB"
            };
            docstore.Initialize();

            // create initial data
            SeedData.CreateUsers(docstore);

            return docstore;
        }
    
    }


}