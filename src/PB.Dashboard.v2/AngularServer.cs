﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Owin;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;
using Owin;

namespace PB.Dashboard
{
    public static class AngularServerExtension
    {
        private static string GetFullRoot(string root)
        {
            string str = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, root));
            if (!str.EndsWith(Path.DirectorySeparatorChar.ToString(), StringComparison.Ordinal))
                str = str + (object)Path.DirectorySeparatorChar;
            return str;
        }

        public static IAppBuilder UseAngularServer(this IAppBuilder builder, string rootPath, string entryPath)
        {
            var options = new AngularServerOptions()
            {
                FileServerOptions = new FileServerOptions()
                {
                    EnableDirectoryBrowsing = false,
                    FileSystem = new PhysicalFileSystem(rootPath)
                },
                EntryPath = new PathString(entryPath),
                FullRoot = GetFullRoot(rootPath)
            };

            builder.UseDefaultFiles(options.FileServerOptions.DefaultFilesOptions);

            return builder.Use<AngularServerMiddleware>(options);
        }
    }

    public class AngularServerOptions
    {
        public FileServerOptions FileServerOptions { get; set; }

        public PathString EntryPath { get; set; }

        public string FullRoot { get; set; }

        public bool Html5Mode
        {
            get
            {
                return EntryPath.HasValue;
            }
        }

        public AngularServerOptions()
        {
            FileServerOptions = new FileServerOptions();
            EntryPath = PathString.Empty;
        }
    }

    public class AngularServerMiddleware
    {
        private readonly AngularServerOptions _options;
        private readonly Func<IDictionary<string, object>, Task> _next;
        private readonly StaticFileMiddleware _innerMiddleware;

        public AngularServerMiddleware(Func<IDictionary<string, object>, Task> next, AngularServerOptions options)
        {
            _next = next;
            _options = options;

            _innerMiddleware = new StaticFileMiddleware(next, options.FileServerOptions.StaticFileOptions);
        }

        public async Task Invoke(IDictionary<string, object> environment)
        {
            // try to resolve the request with default static file middleware
            var context = (IOwinContext)new OwinContext(environment);
            var path = context.Request.Path.Value;
            if (path.StartsWith("/"))
            {
                path = path.Substring(1);
            }
            var fullPath = Path.Combine(_options.FullRoot, path);
            if (!File.Exists(fullPath) && !path.Contains("api"))
            {
                context.Request.Path = _options.EntryPath;
            }

            await _innerMiddleware.Invoke(environment);
            // route to root path if the status code is 404
            // and need support angular html5mode
            //if (context.Response.StatusCode == 404 && _options.Html5Mode)
            //{
            //    try
            //    {
            //        //var response = environment["owin.ResponseBody"] as Stream;
            //        //response.Flush();

            //        //context.Response.Body = new MemoryStream();
            //        context.Request.Path = _options.EntryPath;
            //        await _innerMiddleware.Invoke(environment);
            //    }
            //    catch (Exception ex)
            //    {
            //        var x = ex.Message;
            //    }
            //}
        }
    }
}