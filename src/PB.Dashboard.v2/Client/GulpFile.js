﻿'use strict';
// Generated on 2014-04-14 using generator-leaflet 0.0.14

var gulp = require('gulp');
var wiredep = require('wiredep').stream;
var open = require('open');
var mainBowerFiles = require('main-bower-files');

// Load plugins
var $ = require('gulp-load-plugins')();

// Styles
gulp.task('styles', function () {
    return gulp.src(['app/styles/*.css'])
        .pipe($.autoprefixer('last 1 version'))
        .pipe(gulp.dest('app/styles'))
        .pipe($.size());
});

// Scripts
gulp.task('scripts', function () {
    return gulp.src(['app/scripts/**/*.js'])
       // .pipe($.jshint('.jshintrc'))
       // .pipe($.jshint.reporter('default'))
        .pipe($.size());
});

// HTML
gulp.task('html', ['styles', 'scripts'], function () {
    var jsFilter = $.filter('**/*.js');
    var cssFilter = $.filter('**/*.css');
    var assets = $.useref.assets();

    return gulp.src('app/index.html')
        .pipe(assets)
        //.pipe($.if('*.js', $.uglify()))
        //.pipe($.if('*.css', $.csso()))
        //.pipe(jsFilter)
        //.pipe($.uglify())
        //.pipe(jsFilter.restore())
        //.pipe(cssFilter)
        //.pipe($.csso())
        //.pipe(cssFilter.restore())
        .pipe($.rev())
        .pipe(assets.restore())
        .pipe($.useref())
        .pipe($.revReplace())
        .pipe(gulp.dest('dist'))
        .pipe($.size());
});

// Images
gulp.task('images', function () {
    return gulp.src('app/images/**/*')
        //.pipe($.cache($.imagemin({
        //    optimizationLevel: 3,
        //    progressive: true,
        //    interlaced: true
        //})))
        .pipe(gulp.dest('dist/images'))
        .pipe($.size());
});

// Data
gulp.task('data', function () {
    return gulp.src([
            'app/scripts/**/*.csv',
            'app/scripts/**/*.json',
            'app/scripts/charts.js',
            'app/scripts/chartsOesterdam.js'
        ])
        .pipe(gulp.dest('dist/scripts'))
        .pipe($.size());
});

// Fonts
gulp.task('fonts', function () {
    return gulp.src([
            'app/bower_components/bootstrap/fonts/*.*'])
        .pipe(gulp.dest('dist/fonts'))
        .pipe($.size());
});

// Data
gulp.task('views', function () {
    return gulp.src([
            'app/*.html',
            '!app/index.html'])
        .pipe(gulp.dest('dist'))
        .pipe($.size());
});

// Inject Bower components
gulp.task('bower', function () {
    gulp.src('app/styles/*.css')
        .pipe(wiredep({
            directory: 'app/bower_components',
            ignorePath: 'app/bower_components/'
        }))
        .pipe(gulp.dest('app/styles'));

    gulp.src('app/*.html')
        .pipe(wiredep({
            directory: 'app/bower_components',
            ignorePath: 'app/'
        }))
        .pipe(gulp.dest('app'));
});

// Connect
gulp.task('connect', function () {
    $.connect.server({
        root: ['app'],
        port: 9000,
        livereload: true
    });
});

// Open
gulp.task('serve', ['connect'], function () {
    open("http://localhost:9000");
});

// Watch
gulp.task('watch', ['connect', 'serve'], function () {
    // Watch for changes in `app` folder
    gulp.watch([
        'app/*.html',
        'app/styles/**/*.css',
        'app/scripts/**/*.js',
        'app/images/**/*'
    ], function (event) {
        console.log('reload');
        return gulp.src(event.path)
            .pipe($.connect.reload());
    });

    // Watch .css files
    gulp.watch('app/styles/**/*.css', ['styles']);

    // Watch .js files
    gulp.watch('app/scripts/**/*.js', ['scripts']);

    // Watch image files
    gulp.watch('app/images/**/*', ['images']);

    // Watch bower files
    gulp.watch('bower.json', ['bower']);
});

// pot
gulp.task('pot', function () {
    return gulp.src(['app/**/*.html', 'app/scripts/**/*.js'])
        .pipe($.angularGettext.extract('template.pot', {
            // options to pass to angular-gettext-tools... 
        }))
        .pipe(gulp.dest('po/'));
});

gulp.task('translations', function () {
    return gulp.src('po/*.po')
        .pipe($.angularGettext.compile({
            // options to pass to angular-gettext-tools... 
            format: 'javascript'
        }))
        .pipe(gulp.dest('app/scripts/'));
});

// Build
gulp.task('build', ['html', 'images', 'data', 'fonts', 'views', 'fonts']);

// Clean
gulp.task('clean', function () {
    return gulp.src(['dist/css', 'dist/scripts', 'dist/images', 'dist/fonts'], { read: false }).pipe($.clean());
});

// Default task
gulp.task('default', ['clean'], function () {
    gulp.start('build');
});