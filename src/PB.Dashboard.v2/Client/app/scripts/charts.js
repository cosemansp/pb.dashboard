﻿$(document).ready(function () {
    // temp charts
    var optionsChartTemp = {
        chart: {
            zoomType: 'x',
            renderTo: 'chartTemp',
            type: 'spline'
        },
        title: {
            text: 'Temperature H2O on different depths'
        },
        subtitle: {
            text: 'Muisbroek - Put Van Ekeren'
        },
        xAxis: {
            type: 'datetime',
            //tickInterval: 30 * 24 * 3600 * 1000, // one month
            //tickWidth: 1,
            //gridLineWidth: 1,
        },
        yAxis: {
            title: {
                text: 'Temperature (°C)'
            },
        },
        legend: {
            enabled: true
        },
        tooltip: {
            shared: true,
            valueSuffix: '°C',
            crosshairs: true
        },

        series: [{}]
    };

    // temp charts
    var optionsChartAverageTemp = {
        chart: {
            zoomType: 'x',
            renderTo: 'chartAverageTemp',
            type: 'spline'
        },
        title: {
            text: 'Average temperature H2O'
        },
        subtitle: {
            text: 'Muisbroek - Put Van Ekeren'
        },
        xAxis: {
            type: 'datetime',
        },
        yAxis: {
            title: {
                text: 'Temperature (°C)'
            },
        },
        tooltip: {
            shared: true,
            valueSuffix: '°C',
            crosshairs: true
        },
        legend: {
            enabled: true
        },
        series: [{}]
    };

    // viz charts
    var optionsChartViz = {
        chart: {
            zoomType: 'x',
            renderTo: 'chartViz',
            type: 'spline'
        },
        title: {
            text: 'Visibility on different depths'
        },
        subtitle: {
            text: 'Muisbroek - Put Van Ekeren'
        },
        xAxis: {
            type: 'datetime',
        },
        yAxis: {
            title: {
                text: 'Visibility'
            },
        },
        tooltip: {
            shared: true,
            crosshairs: true
        },
        legend: {
            enabled: true
        },
        series: [{}]
    };

    // temp charts
    var optionsChartAverageViz = {
        chart: {
            zoomType: 'x',
            renderTo: 'chartAverageViz',
            type: 'spline'
        },
        title: {
            text: 'Average visibility H2O'
        },
        subtitle: {
            text: 'Muisbroek - Put Van Ekeren'
        },
        xAxis: {
            type: 'datetime',
        },
        yAxis: {
            title: {
                text: 'Visibility (m)'
            },
        },
        tooltip: {
            shared: true,
            crosshairs: true
        },
        legend: {
            enabled: true
        },
        series: [{}]
    };
    $.getJSON('/api/chartTemp?regio=muisbroek&callback=?', function (data) {
        optionsChartTemp.series = data;
        var chart = new Highcharts.Chart(optionsChartTemp);
    });
    $.getJSON('/api/chartViz?regio=muisbroek&callback=?', function (data) {
        optionsChartViz.series = data;
        var chart = new Highcharts.Chart(optionsChartViz);
    });
    $.getJSON('/api/chartAverageTemp?regio=muisbroek&callback=?', function (data) {
        optionsChartAverageTemp.series = data;
        var chart = new Highcharts.Chart(optionsChartAverageTemp);
    });
    $.getJSON('/api/chartAverageViz?regio=muisbroek&callback=?', function (data) {
        optionsChartAverageViz.series = data;
        var chart = new Highcharts.Chart(optionsChartAverageViz);
    });
});
