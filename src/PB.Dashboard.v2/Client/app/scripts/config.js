﻿angular.module('app')
    .config(function($httpProvider, RestangularProvider) {

        RestangularProvider.setBaseUrl("/api");

    })
    .config([
        '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false,
            });

            $routeProvider
                .when('/', {
                    templateUrl: 'dashboard.html',
                    controller: 'dashboardController'
                })
                .when('/samples', {
                    templateUrl: 'samples.html',
                    controller: 'samplesController'
                })
                .when('/sample-edit/:id?', {
                    templateUrl: 'sample-edit.html',
                    controller: 'sampleEditController'
                })
                .when('/species-inventory', {
                    templateUrl: 'species-inventory.html',
                    controller: 'speciesInventoryController'
                })
                .when('/species-inventory-edit/:id?', {
                    templateUrl: 'species-inventory-edit.html',
                    controller: 'speciesInventoryEditController'
                })
                .when('/users', {
                    templateUrl: 'users.html',
                    controller: 'usersController'
                })
                .when('/charts-muisbroek', {
                    templateUrl: 'charts-muisbroek.html',
                    controller: 'chartController'
                })
                .when('/charts-oesterdam', {
                    templateUrl: 'charts-oesterdam.html',
                    controller: 'chartController'
                })
                .otherwise({
                    redirectTo: '/'
                });
        }
    ])
    .config(function(dialogsProvider) {
        dialogsProvider.setSize('sm');
    })
    // Exception Handler
    .config(exceptionHandler)
    // Http interceptor
    .config(function ($httpProvider) {
        $httpProvider.interceptors.push('httpInterceptor');
    })
    // Bind Alert Event
    .run(bindAlertError)
    .run(function (gettextCatalog, $window, localStorage) {
        var language = localStorage.get('language');
        if (!language) {
            // use the $window service to get the language of the user's browser
            language = $window.navigator.languages ? $window.navigator.languages[0] :
                       ($window.navigator.language || $window.navigator.userLanguage);
        }
        if (language.indexOf('nl') == 0) {
            gettextCatalog.setCurrentLanguage('nl');
        }
        else {
            gettextCatalog.setCurrentLanguage('en');
        }
        gettextCatalog.debug = true;
    })
    .run(function () {
        // setup locale for datetimepicker
        moment.locale('nl', {
            longDateFormat: {
                LT: 'HH:mm',
                LTS: 'LT:ss',
                L: 'DD-MM-YYYY',
                LL: 'D MMMM YYYY',
                LLL: 'D MMMM YYYY LT',
                LLLL: 'dddd D MMMM YYYY LT'
            }
        });
    })
    .factory('httpInterceptor', function httpInterceptor($q, $log, toaster) {

        var responseError = function(response) {

            // Status codes not shown in toastr
            var ignoredHttpStatusCodes = [409];
            if (_.contains(ignoredHttpStatusCodes, response.status)) {
                // default handling, pass error to caller
                return $q.reject(response);
            }

            $log.info('Response received with HTTP status code: ' + response.status);

            if (response.status === 0) {
                toaster.pop('warning', 'Oops, Something went wrong', 'The connection to the server is lost.');
                return $q.reject(response); // stop promise chain
            }

            if (response.status >= 400 && response.status < 500) {
                var msg;
                switch (response.status) {
                case 403:
                case 401:
                    msg = 'Authentication credentials were missing or incorrect [401].';
                    break;
                case 404:
                    msg = 'The URI requested is invalid or the resource requested does not exists [404].';
                    break;
                default:
                    msg = 'The request was invalid or cannot be otherwise served [' + response.status + '].';
                    break;
                }
                toaster.pop('warning', 'Oops, Something went wrong', msg);
                return $q.reject(response); // stop promise chain
            }

            if (response.status >= 500) {
                toaster.pop('error', 'Whaw, unexpected response', 'Something is broken at the server [' + response.status + '].');
                return $q.reject(response); // stop promise chain
            }

            return $q.reject(response);
        };

        return {
            responseError: responseError
        };
    });

function exceptionHandlerDecorator($delegate, $injector) {
    return function (exception, cause) {
        var scope = $injector.get('$rootScope');
        var message = exception && exception.message;
        scope.$emit("_alertEvent", 'Program Error', message);
        $delegate(exception, cause);
    };
}

function exceptionHandler($provide) {
    $provide.decorator('$exceptionHandler', exceptionHandlerDecorator);
}

function bindAlertError($rootScope, toaster) {
    $rootScope.$on("_alertEvent", function (event1, message, detail) {
        toaster.pop('error', message, detail);
    });
}
