﻿angular.module('app')
    .directive('dateFormat', function ($filter) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                var dateFormat = attrs['dateFormat'] || 'yyyy-MM-dd';

                ngModel.$formatters.unshift(function (viewValue) {
                    //console.log(viewValue);
                    if (!angular.isDefined(viewValue))
                        return undefined;
                    var now = moment(viewValue);
                    var formattedDate = $filter('date')(now.toDate(), dateFormat);
                    //console.log(formattedDate);
                    return formattedDate;
                });
            }
        }
    });