﻿(function () {

    var specieNumbers = [
        { id: '999', label: 'Unkown', number: '?' },
        { id: '0', label: 'None', number: '0' },
        { id: '1', label: 'One', number: '1' },
        { id: '2', label: 'Some (2 - 4)', number: '2-4' },
        { id: '3', label: 'Many (5 - 99)', number: '5-99' },
        { id: '4', label: 'Massive (+ 100)', number: '+100' }
    ];

    var specieSizes = [
        { id: '5', label: 'Juvenile' },
        { id: '10', label: 'Adult' },
        { id: '20', label: 'Large Adult' }
    ];

    function parseLocation(location) {
        var pairs = location.substring(1).split("&");
        var obj = { };
        var pair;
        var i;

        for (i in pairs) {
          if (pairs[i]=== "") continue;
          pair = pairs[i].split("=");
          obj[decodeURIComponent(pair[0])]= decodeURIComponent(pair[1]);
        }

        return obj;
    };

angular.module('app')
    .controller('headerController', function ($scope, $window, $location,
                                              toaster, authService, gettextCatalog,
                                              localStorage) {
        var query = parseLocation($window.location.search);
        if (angular.isDefined(query.message)) {
            toaster.pop('info', "Notification", query.message, 5000);
            $location.search('message', null);
            $location.search('email', null);
            $location.search('success', null);
            $location.path("/");
        }
        var language = gettextCatalog.getCurrentLanguage();
        $scope.language = "English";
        if (language == 'nl') {
            $scope.language = "Nederlands";
        }

        $scope.login = function() {
            authService.authenticate()
                .then(function(identity) {
                });
        }

        $scope.logout = function() {
            authService.logout();
        }

        $scope.setLanguage = function (lang) {
            console.log('setLanguage', lang);
            gettextCatalog.setCurrentLanguage(lang);
            $scope.language = "English";
            if (lang == 'nl') {
                $scope.language = "Nederlands";
            }
            localStorage.add('language', lang);
        }

    })
    .controller('navController', function($scope, authService, $location) {
        $scope.logout = function() {
            authService.logout();
            $location.path("/");
        }
    })
    .controller('dashboardController', function($scope, Restangular) {
        $scope.stats = {};
        $scope.stats.measurements = 0;
        $scope.stats.users = 0;
        $scope.stats.speciesInventories = 0;

        Restangular.one('stats').get()
            .then(function(stats) {
                $scope.stats = stats;
            });

    })
    .controller('samplesController', function ($scope, Restangular, $location, dialogs) {
        $scope.measurements = Restangular.all('measurement').getList().$object;
        $scope.deleteItem = function (item) {
            var dlg = dialogs.confirm("Confirm", "Are you sure you want to delete this measurement?");
            dlg.result.then(function (btn) {
                item.remove().then(function() {
                    $scope.measurements.splice($scope.measurements.indexOf(item), 1);
                });
            }, function (btn) {
                // noop
            });
        }
        $scope.addnew = function() {
            $location.path("sample-edit");
        }
    })
    .controller('sampleEditController', function ($scope, $routeParams, Restangular, $location) {
        var id = $routeParams.id;
        $scope.regios = [
            { name: 'Muisbroek', zone:'Grote Boei', alias: 'Muisbroek - Put Van Ekeren'},
            { name: 'Oesterdam', zone: '', alias: 'Oesterdam / Berghse Diepsluis' }
        ];
        if (id != null) {
            Restangular.one('measurement', id).get()
                .then(function (sample) {
                    $scope.sample = sample;
                    return Restangular.all('user').getList({ filter: "publisher" });
                })
                .then(function (users) {
                    $scope.users = users;
                    $scope.who = $scope.sample.who;
                    $scope.place = $scope.sample.place;
                });
        } else {
            $scope.users = Restangular.all('user').getList({ filter: "publisher" }).$object;
            $scope.sample = Restangular.one('measurement'); // create new one
            $scope.sample.dateTime = new Date();
        }

        $scope.submit = function () {
            console.log('where', $scope.place);
            $scope.sample.who = $scope.who;
            $scope.sample.place = $scope.place;
            if ($scope.place.toLowerCase() != 'muisbroek') {
                $scope.sample.zone = 'Platform';
            }
            $scope.sample.save();
            $location.path("/samples");
        };
    })
    .controller('speciesInventoryController', function ($scope, $rootScope, Restangular, $location, dialogs) {
        Restangular.all('speciesInventory').getList()
            .then(function(inventories) {
                $scope.inventories = _.map(inventories, function(item) {
                    var species = '';
                    _.each(item.observations, function (observation) {
                        console.log(observation.number);
                        var num = _.findWhere(specieNumbers, { id: observation.number });
                        species += observation.specie + ' (' + num.number + "), ";
                    });
                    return {
                        id: item.id,
                        dateTime: item.dateTime,
                        location: item.location,
                        diveTime: item.diveTime,
                        waterTemp: item.waterTemp,
                        userName: item.userName,
                        userId: item.userId,
                        comments: item.comments,
                        species: species
                    };
                });
            });

        $scope.canEdit = function (item) {
            var identity = $rootScope.identity;
            return (identity.userId === item.userId || identity.isInRole('admin'));
        }

        $scope.deleteItem = function (item) {

            var dlg = dialogs.confirm("Confirm", "Are you sure you want to delete this inventory?");
            dlg.result.then(function (btn) {
                Restangular.one("speciesInventory", item.id).remove()
                .then(function () {
                    $scope.inventories.splice($scope.inventories.indexOf(item), 1);
                });
            }, function (btn) {
                // noop
            });
        }

        $scope.addnew = function () {
            $location.path("species-inventory-edit");
        }
    })
    .controller('speciesInventoryEditController', function ($scope, $rootScope, $routeParams, Restangular, $location) {
        var id = $routeParams.id;
        var emptySpecie = { name: "", label: "", image: 'select-specie.jpg' };
        $scope.species = [
            { name: "Perch", label: "Perch - Baars - Perche", image: 'perch.jpg' },
            { name: "Eel", label: "Eel - Paling - Anguille", image: 'eel.jpg' },
            { name: "Tench", label: "Tench - Zeelt - Tanche", image: 'tench.jpg' },
            { name: "Carp", label: "Carp - Karper - Carpe", image: 'carp.jpg' },
            { name: "Ruffe", label: "Ruffe - Pos - Ruffe", image: 'ruffe.jpg' },
            { name: "Pike", label: "Pike - Snoek - Brochet", image: 'pike.jpg' },
            { name: "Rudd", label: "Rudd - Ruisvoorn - Rudd", image: 'rudd.jpg' },
            { name: "Bitterling", label: "Bitterling - Bittervoorn - Bouvière", image: 'bitterling.jpg' },
            { name: "Bream", label: "Bream - Brasem - Brème", image: 'bream.jpg' },
            { name: "PikePerch", label: "Pike-Perch - Snoekbaars - Sandre", image: 'pike-perch.png'},
            { name: "Roach", label: "Roach - Blankvoorn - Gardon", image: 'roach.png' },
            { name: "WeatherLoach", label: "Weather Loach - Modderkruiper - Brème", image: 'weather-loach.png' },
            { name: "CrayFlish", label: "CrayFlish - Rivierkreeft - Ecrevisse", image: 'cray-fish.jpg' }
        ];
        $scope.locations = ['Muisbroek', 'Bos van Aa'];
        $scope.numbers = specieNumbers;
        $scope.sizes = specieSizes;

        $scope.item = {};
        $scope.item.isOwner = true;
        
        if (id != null) {
            Restangular.one('speciesInventory', id).get()
                .then(function(item) {
                    $scope.rawItem = item;
                    $scope.item.id = item.id;
                    $scope.item.dateTime = item.dateTime;
                    $scope.item.diveTime = item.diveTime;
                    $scope.item.waterTemp = item.waterTemp;
                    $scope.item.comments = item.comments;
                    $scope.item.observations = _.map(item.observations, function(observation) {
                        return {
                            specie: _.find($scope.species, function(item) {
                                return item.name === observation.specie;
                            }),
                            number: observation.number,
                            size: observation.size
                        };
                    });
                    $scope.item.isOwner = (!angular.isDefined($scope.rawItem.userId) || $scope.rawItem.userId == $rootScope.identity.userId);
                });
        }
        else {
            $scope.rawItem = Restangular.one('speciesInventory'); // create new one
            $scope.item.observations = [
                { specie: emptySpecie, number: '' }
            ];
            $scope.item.dateTime = new Date();
            $scope.item.isOwner = true;
        }

        $scope.addObservation = function () {
            if ($scope.item.observations.length < 11)
                $scope.item.observations.push({ specie: emptySpecie, number: '' });
        };

        $scope.removeObservation = function(observation) {
            $scope.item.observations.splice($scope.item.observations.indexOf(observation), 1);
        };

        $scope.specieChanged = function(observation) {
            observation.number = "1";
        }

        $scope.submit = function () {
            $scope.rawItem.dateTime = $scope.item.dateTime;
            $scope.rawItem.location = $scope.item.location;
            $scope.rawItem.waterTemp = $scope.item.waterTemp;
            $scope.rawItem.diveTime = $scope.item.diveTime;
            $scope.rawItem.comments = $scope.item.comments;
            $scope.rawItem.observations = _.map($scope.item.observations, function (observation) {
                return {
                    specie: observation.specie.name,
                    number: observation.number,
                    size: observation.size
                }
            });
            $scope.rawItem.save()
                .then(function() {
                    $location.path("/species-inventory");
                });
        };
    })
    .controller('usersController', function ($scope, Restangular) {
        $scope.users = Restangular.all('user').getList().$object;
    })
    .controller('chartController', function($scope) {
        
    });
})();