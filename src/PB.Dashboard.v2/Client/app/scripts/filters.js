﻿angular.module('app')
    .filter('meter', function () {
        return function (input) {
            if (input == null)
                return "-";
            return input + "m";
        };
    })
    .filter('temp', function () {
        return function (input) {
            if (input == null)
                return "-";
            return input + "°C";
        };
    });