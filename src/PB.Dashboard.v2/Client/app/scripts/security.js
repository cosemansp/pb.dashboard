﻿(function (angular) {
    // ReSharper disable 

    'use strict'

    var events = {
        userLoggedIn: "userLoggedIn",
        userLoggedOut: "userLoggedOut"
    };

    angular.module('app')
        .constant("events", events)
        .config(configAuth0)
        .factory('Identity', Identity)
        .service('authService', AuthService)
        .service('sessionStorage', SessionStorage)
        .service('localStorage', LocalStorage)
        .run(loadIdentity);

    function configAuth0($httpProvider, authProvider, jwtInterceptorProvider) {

        // init auth0
        authProvider.init({
            domain: 'pb-dashboard.auth0.com',
            clientID: 'tZjseYmnuxOW01nmU4VpTB6jlxwm1YOG',
            loginUrl: '/login'
        });

        // init tokenGetter
        jwtInterceptorProvider.tokenGetter = function (sessionStorage) {
            return sessionStorage.get('token');
        }

        $httpProvider.interceptors.push('jwtInterceptor');
    }

    function SessionStorage($window, $cookieStore) {
        var hasSessionStorage = testSessionStorage();

        this.add = function (key, value) {
            if (hasSessionStorage) {
                return $window.sessionStorage.setItem(key, JSON.stringify(value));
            }
            return $cookieStore.put(key, value);
        }
        this.remove = function (key) {
            if (hasSessionStorage) {
                return $window.sessionStorage.removeItem(key);
            }
            $cookieStore.remove(key);
        }
        this.get = function (key) {
            if (hasSessionStorage) {
                var value = $window.sessionStorage.getItem(key);
                return value && JSON.parse(value);
            }
            return $cookieStore.get(key);
        }

        function testSessionStorage() {
            try {
                $window.sessionStorage.setItem('angular.sessionStorage.test', true);
                $window.sessionStorage.removeItem('angular.sessionStorage.test');
                return true;
            } catch (e) {
                return false;
            }
        }
    }

    function LocalStorage($window, $cookieStore) {
        var hasLocalStorage = testLocalStorage();

        this.add = function (key, value) {
            if (hasLocalStorage) {
                return $window.localStorage.setItem(key, JSON.stringify(value));
            }
        }
        this.remove = function (key) {
            if (hasLocalStorage) {
                return $window.localStorage.removeItem(key);
            }
        }
        this.get = function (key) {
            if (hasLocalStorage) {
                var value = $window.localStorage.getItem(key);
                return value && JSON.parse(value);
            }
        }

        function testLocalStorage() {
            try {
                $window.localStorage.setItem('angular.localStorage.test', true);
                $window.localStorage.removeItem('angular.localStorage.test');
                return true;
            } catch (e) {
                return false;
            }
        }
    }

    function AuthService(sessionStorage, Identity, $rootScope, $log, $q, events, auth) {

        this.authenticate = function () {

            // prepare
            var deferred = $q.defer();

            // sign-in with auth0
            auth.signin({}, function (profile, token) {

                // save token and profile to session
                sessionStorage.add("token", token);
                sessionStorage.add("profile", profile);

                // set identity 
                var identity = Identity.build(profile);
                $rootScope.identity = identity;

                // notify others
                $log.log("Successfull logged in: ", identity);
                $rootScope.$emit(events.userLoggedIn, identity);

                // resolve promise as success
                deferred.resolve(identity);

            }, function (error) {
                $log.log("There was an error logging in", error);
                $rootScope.identity = Identity.anonymous;

                // reject promise
                deferred.reject(error);
            });
            return deferred.promise;
        }

        this.logout = function () {
            // remove token
            sessionStorage.remove("token");
            sessionStorage.remove("profile");

            // set anonymous identity
            $rootScope.identity = Identity.anonymous;

            // log and notity to others
            $log.info("Logged out.");
            $rootScope.$emit(events.userLoggedOut);
        }

        this.init = function () {
            var profile = sessionStorage.get("profile");

            if (profile != null) {
                return Identity.build(profile);
            }
            return Identity.anonymous;
        }
    }

    function loadIdentity($rootScope, authService) {
        $rootScope.identity = authService.init();
    }

    function Identity() {

        var clazz = function (profile) {
            if (!angular.isDefined(profile)) {
                this.name = "anonymous";
                this.userId = "?";
                this.roles = "";
                return;
            }
            this.name = profile.name;
            this.email = profile.email;
            this.roles = angular.isDefined(profile.roles) ? profile.roles : "user";
            this.userId = profile.user_id;
            this.picture = profile.picture;
            this.givenName = profile.given_name;
            this.authenticated = angular.isDefined(profile.user_id);
            if (!this.authenticated)
                this.roles = null;
        }

        clazz.prototype.isInRole = function (role) {
            if (!this.authenticated)
                return false;

            return this.roles.indexOf(role) != -1;
        }

        clazz.prototype.isAuthenticated = function () {
            return this.authenticated;
        }

        clazz.possibleRoles = ['admin', 'publisher', 'user'];

        clazz.build = function (profile) {
            return new clazz(profile);
        };

        clazz.anonymous = new clazz();

        return clazz;
    }

    // ReSharper restore 
})(angular);