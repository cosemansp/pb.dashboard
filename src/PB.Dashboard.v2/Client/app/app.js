﻿angular.module('app', [
    'ngRoute',
    'ngCookies',
    'auth0',
    'ui.bootstrap',
    'ui.bootstrap.datetimepicker',
    'restangular',
    'angular-jwt',
    'angular-loading-bar',
    'ngAnimate',
    'toaster',
    'dialogs.main',
    'gettext'
    ]
);

