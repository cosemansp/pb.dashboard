﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PB.Dashboard.Domain.Entities
{
    public class Measurement
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Who { get; set; }
        public string UserId { get; set; }
        public string Place { get; set; }
        public string Zone { get; set; }
        public float TempAir { get; set; }
        public List<Sample> Samples { get; set; }

        public Measurement()
        {
            Samples = new List<Sample>();
        }

        public Sample GetSample(int dept)
        {
            var sample = Samples.FirstOrDefault(x => x.Dept == dept);
            if (sample == null)
                return new Sample();
            return sample;
        }

        public string GetTempAir()
        {
            if (TempAir > -99.0)
                return TempAir.ToString("#.#");
            return null;
        }
    }
}