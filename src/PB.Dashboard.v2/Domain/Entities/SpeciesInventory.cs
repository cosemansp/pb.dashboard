﻿using System;
using System.Collections.Generic;

namespace PB.Dashboard.Domain.Entities
{
    public enum Specie
    {
        Perch,      // Baars, Perche
        Eel,        // Paling, Anguille
        Tench,      // Zeelt, Tanche
        Carp,       // Karper, Carpe
        Ruffe,      // Pos, Ruffe
        Pike,       // Snoek, Brochet
        Rudd,       // Ruisvoorn, Rudd
        Bitterling, // Bittervoorn, Bouvière
        Bream,      // Brasem, Brème
        CrayFlish,     // Rivierkreeft, écrevisse

        PikePerch,    // Snoekbaars - Sandre
        WeatherLoach, // Modderkruiper - Brème
        Roach          // Blankvoorn - Gardon
    }

    public class Observation
    {
        public Specie Specie { get; set; }
        public string Number { get; set; }
        public string Size { get; set; }
    }

    public class SpeciesInventory
    {
        // ReSharper disable InconsistentNaming
        public int Id { get; set; }
        public DateTime DateTime { get; set; }
        public string Location { get; set; }
        public string UserId { get; set; }
        public float WaterTemp { get; set; }
        public int DiveTime { get; set; }
        public string Comments { get; set; }
        // ReSharper enable InconsistentNaming

        public List<Observation> Observations { get; set; }

        public SpeciesInventory()
        {
            Observations = new List<Observation>();
        }
    }
}