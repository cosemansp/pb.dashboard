﻿using System.Collections.Generic;
namespace PB.Dashboard.Domain.Entities
{
    public static class UserExtensions
    {
        public static bool IsInRole(this User user, string role)
        {
            if (user == null)
                return false;

            if (user.Role.Contains(role))
                return true;

            return false;
        }
    }


    public class User
    {
        public User()
        {
            UserIds = new List<string>();
        }

        public string Id { get; set; }

        // Social properties
        public string Name { get; set; }
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
        public string Picture { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public List<string> UserIds { get; set; }

        public string NickName { get; set; }

        /// <summary>
        /// The role of the user:
        /// - User
        /// - Publisher
        /// - Admin
        /// </summary>
        public string Role { get; set; }
    }
}