﻿namespace PB.Dashboard.Domain.Entities
{
    public class Sample
    {
        public int Dept { get; set; }
        public float Temp { get; set; }
        public float Viz { get; set; }
    }
}