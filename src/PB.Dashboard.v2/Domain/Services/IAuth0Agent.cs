﻿using Auth0;

namespace PB.Dashboard.Domain.Services
{
    public interface IAuth0Agent
    {
        UserProfile GetUser(string userId);
    }
}