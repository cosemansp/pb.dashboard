﻿using System;
using System.Web;
using System.Web.Caching;

namespace PB.Dashboard.Domain.Services
{
    public interface ICache
    {
        void Clear();
        object Get(string key);
        void Remove(string key);
        void Store(string key, object value, int minutesValidFor);
    }

    public class LocalCache : ICache
    {
        public object Get(string key)
        {
            return HttpRuntime.Cache.Get(key);
        }

        public void Store(string key, object value, int minutesValidFor)
        {
            HttpRuntime.Cache
                        .Insert(
                        key,
                        value,
                        null,
                        Cache.NoAbsoluteExpiration,
                        TimeSpan.FromMinutes(minutesValidFor),
                        CacheItemPriority.Normal,
                        ItemRemoved);
        }

        public void Remove(string key)
        {
            HttpRuntime.Cache.Remove(key);
        }

        public void Clear()
        {
            var enumerator = HttpRuntime.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                HttpRuntime.Cache.Remove(enumerator.Key.ToString());
            }
        }

        private void ItemRemoved(string key, object value, CacheItemRemovedReason reason)
        {
            //TODO: Log removal
        }
    } 
}