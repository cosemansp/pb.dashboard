﻿using Auth0;

namespace PB.Dashboard.Domain.Services
{
    public class Auth0Agent : IAuth0Agent
    {
        private readonly Client _client;

        public Auth0Agent(string clientId, string clientSecret, string domain)
        {
            _client = new Client(clientId, clientSecret, domain);
        }

        public UserProfile GetUser(string userId)
        {
            return _client.GetUser(userId);
        }
    }
}