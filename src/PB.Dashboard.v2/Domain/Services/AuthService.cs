﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using PB.Dashboard.Domain.Entities;
using Raven.Client;
using Raven.Client.Linq;

namespace PB.Dashboard.Domain.Services
{
    public interface IAuthService
    {
        User GetCurrentUser();
    }

    public class AuthService : IAuthService
    {
        private readonly IDocumentSession _session;
        private readonly IAuth0Agent _auth0Agent;
        private readonly ICache _cache;

        public AuthService(IDocumentSession session, IAuth0Agent auth0Agent, ICache cache)
        {
            _session = session;
            _auth0Agent = auth0Agent;
            _cache = cache;
        }

        public User GetCurrentUser()
        {
            var identity = ClaimsPrincipal.Current.Identity.Name;
            if (string.IsNullOrEmpty(identity))
                return null;

            // get user from cache
            var cachedUser = _cache.Get(identity) as User;
            if (cachedUser != null)
                return cachedUser;

            // no cache, get user from auth0 & lookup from db
            var userProfile = _auth0Agent.GetUser(identity);
            if (userProfile == null)
                return null;

            var userRole = "user";
            if (userProfile.ExtraProperties.ContainsKey("roles"))
                userRole = userProfile.ExtraProperties["roles"] as string;
            var user = _session.Query<User>().Where(x => x.UserIds.Any(id => id == identity)).SingleOrDefault();
            if (user == null)
            {
                // try to find, based on name
                user = _session.Query<User>().Where(x => x.Name == userProfile.Name).SingleOrDefault();

                // still not found, create it
                if (user == null || user.UserIds.Count > 0)
                {
                    // user doesn't exist, or it is already in use, add new one
                    user = new User
                    {
                        Name = userProfile.Name,
                        GivenName = userProfile.GivenName,
                        FamilyName = userProfile.FamilyName,
                        Gender = userProfile.Gender,
                        Picture = userProfile.Picture,
                        NickName = userProfile.GivenName,
                        Email = userProfile.Email,
                        Role = userRole,
                        UserIds = new List<string> { userProfile.UserId }
                    };
                }
                else
                {
                    // update user
                    user.GivenName = userProfile.GivenName;
                    user.FamilyName = userProfile.FamilyName;
                    user.Gender = userProfile.Gender;
                    user.Picture = userProfile.Picture;
                    user.Email = userProfile.Email;
                    user.UserIds.Add(userProfile.UserId);
                }
                _session.Store(user);
                _session.SaveChanges();
            }

            if (userRole != null && userRole != user.Role)
            {
                user.Role = userRole;
                _session.Store(user);
                _session.SaveChanges();
            }

            // store in cache for faster retrieval
            _cache.Store(identity, user, 30);

            // return
            return user;
        }
    }
}