﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.SessionState;
using PB.Dashboard.Domain.Entities;
using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Linq;

namespace PB.Dashboard.Data
{
    public static class SeedData
    {
        public static void CreateUsers(DocumentStore store)
        {
            using (var session = store.OpenSession())
            {
                var numberOfUsers = session.Query<User>().Count();
                if (numberOfUsers == 0)
                {
                    // create all initial users
                    session.Store(new User { Name = "Peter Cosemans", NickName = "Peter", Role = "admin, publisher, user"});
                    session.Store(new User { Name = "Ben van Asselt", NickName = "Ben", Role = "publisher, user" });
                    session.Store(new User { Name = "Van Schuylenbergh", NickName = "Koenraad", Role = "publisher, user" });
                    session.Store(new User { Name = "Jonas Patteet", NickName = "Jonas", Role = "publisher, user" });
                    session.Store(new User { Name = "Mathias Trappeniers", NickName = "Mathias", Role = "publisher, user" });
                    session.Store(new User { Name = "", NickName = "Roel", Role = "publisher, user" });
                    session.Store(new User { Name = "Van Velthoven", NickName = "Serge", Role = "publisher, user" });
                    session.Store(new User { Name = "Luc Torfs", NickName = "Luc", Role = "publisher, user" });
                }

                //var numberOfSpeciesInventory = session.Query<SpeciesInventory>().Count();
                //if (numberOfSpeciesInventory == 0)
                //{
                //    session.Store(new SpeciesInventory
                //    {
                //        DateTime = DateTime.Now,
                //        Location = "Muisbroek",
                //        WaterTemp = 12.2F,
                //        DiveTime = 55,
                //        Observations = new List<Observation>
                //        {
                //            new Observation { Specie = Specie.Bream, Number = "100+"},
                //            new Observation { Specie = Specie.Eel, Number = "1"},
                //            new Observation { Specie = Specie.CrayFlish, Number = "1"},
                //        }
                //    });

                //    session.Store(new SpeciesInventory
                //    {
                //        DateTime = DateTime.Now - TimeSpan.FromDays(10),
                //        Location = "Muisbroek",
                //        WaterTemp = 5.0F,
                //        DiveTime = 30,
                //        Observations = new List<Observation>
                //        {
                //            new Observation { Specie = Specie.Bream, Number = "11-100"},
                //            new Observation { Specie = Specie.Carp, Number = "2-5"},
                //        }
                //    });
                //}

                session.SaveChanges();
            }
        } 
    }
}