﻿using System.Configuration;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using Autofac.Integration.WebApi;
using Microsoft.Owin;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.StaticFiles;
using Newtonsoft.Json.Serialization;
using NLog;
using Owin;
using PB.Dashboard;
using PB.Dashboard.Core;
using PB.Dashboard.Api;
using WebApiContrib.Formatting.Jsonp;

[assembly: OwinStartup(typeof(Startup))]
namespace PB.Dashboard
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var log = LogManager.GetCurrentClassLogger();
            log.Info("App startup");

            // Ioc container
            var container = Bootstrapper.Configure();

            // use Auth0 JWT Bearer Token
            var issuer = "https://" + ConfigurationManager.AppSettings["auth0:Domain"] + "/";
            var audience = ConfigurationManager.AppSettings["auth0:ClientId"];
            var secret = TextEncodings.Base64.Encode(TextEncodings.Base64Url.Decode(ConfigurationManager.AppSettings["auth0:ClientSecret"]));
            app.UseJwtBearerAuthentication(new JwtBearerAuthenticationOptions
            {
                AuthenticationMode = Microsoft.Owin.Security.AuthenticationMode.Active,
                AllowedAudiences = new[] { audience },
                IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                {
                    new SymmetricKeyIssuerSecurityTokenProvider(issuer, secret)
                }
            });

//            // use simple content files
//            var options = new FileServerOptions
//            {
//#if DEBUG
//                FileSystem = new PhysicalFileSystem(@"Client\app"),
//                EnableDefaultFiles = true,
//                EnableDirectoryBrowsing = true,
                
//#else
//                FileSystem = new PhysicalFileSystem(@"Client\dist"),
//                EnableDefaultFiles = true,
//                EnableDirectoryBrowsing = false
//#endif
//            };
//            app.UseFileServer(options);
#if DEBUG
            app.UseAngularServer(@"Client\app", "/index.html");
#else
            app.UseAngularServer(@"Client\dist", "/index.html");
#endif

            // use Cors 
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            // use OAuth2 JWT token
            var jwtOptions = new JwtOwinAuthOptions();
            app.UseJwtOwinAuth(jwtOptions);

            // use webAPI
            var httpConfig = new HttpConfiguration();
            httpConfig.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            httpConfig.MapHttpAttributeRoutes();
            httpConfig.Formatters.Add(new TextPlainFormatter("text/csv"));
            httpConfig.Formatters.Add(new JsonpMediaTypeFormatter(new JsonMediaTypeFormatter(), "callback"));
            httpConfig.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            httpConfig.Services.Add(typeof(IExceptionLogger), new NLogExceptionLogger());
            app.UseWebApi(httpConfig);
        }
    }
}