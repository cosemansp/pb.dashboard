﻿using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace PB.Dashboard.Api
{
    public class JsonWebTokenClaims : Dictionary<string, object>
    {
        public DateTime ExpireTime
        {
            get
            {
                if (ContainsKey("exp"))
                {
                    var expireTimeInSeconds = (int)this["exp"];
                    var utc0 = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                    return utc0.AddSeconds(expireTimeInSeconds);
                }
                return DateTime.MaxValue;
            }
        }

        public bool IsExpired
        {
            get { return (ExpireTime < DateTime.Now); }
        }

        public ClaimsIdentity ToClaimsIdentity()
        {
            var claims = new List<Claim>();
            var userId = this["sub"] as String;
            claims.Add(new Claim("name", userId));
            claims.Add(new Claim("role", "user"));
            return new ClaimsIdentity(claims, "oauth", "name", "role");
        }
    }
}