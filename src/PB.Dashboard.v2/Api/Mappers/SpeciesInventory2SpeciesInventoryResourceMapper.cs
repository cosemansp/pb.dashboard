using System.Collections.Generic;
using System.Linq;
using PB.Dashboard.Api.Resources;
using PB.Dashboard.Core;
using PB.Dashboard.Domain.Entities;
using Raven.Client;

namespace PB.Dashboard.Api.Mappers
{
    public class SpeciesInventory2SpeciesInventoryResourceMapper : IMapper<SpeciesInventory, SpeciesInventoryResource>
    {
        private readonly IDocumentSession _session;

        public SpeciesInventory2SpeciesInventoryResourceMapper(IDocumentSession session)
        {
            _session = session;
        }

        public SpeciesInventoryResource Map(SpeciesInventory source)
        {
            var dest = new SpeciesInventoryResource();
            return Map(source, dest);
        }

        public SpeciesInventoryResource Map(SpeciesInventory source, SpeciesInventoryResource dest)
        {
            User user = null;
            if (!string.IsNullOrEmpty(source.UserId)) 
                user = _session.Load<User>(source.UserId);

            dest.Id = source.Id;
            dest.Location = source.Location;
            dest.WaterTemp = source.WaterTemp;
            dest.DateTime = source.DateTime;
            dest.UserName = (user != null) ? user.Name : "";
            dest.UserId = (user != null && user.UserIds.Count > 0) ? user.UserIds[0] : "";
            dest.DiveTime = source.DiveTime;
            dest.Comments = source.Comments;
            dest.Observations = new List<ObservationResource>();
            if (source.Observations != null)
            {
                foreach (var observation in source.Observations)
                {
                    dest.Observations.Add(new ObservationResource
                    {
                        Specie = observation.Specie.ToString(),
                        Number = observation.Number,
                        Size = observation.Size
                    });
                }
            }
            return dest;
        }
    }
}