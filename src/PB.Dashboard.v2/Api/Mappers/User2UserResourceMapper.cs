﻿using PB.Dashboard.Api.Resources;
using PB.Dashboard.Core;
using PB.Dashboard.Domain.Entities;

namespace PB.Dashboard.Api.Mappers
{
    public class User2UserResourceMapper : IMapper<User, UserResource>
    {
        public UserResource Map(User source)
        {
            var dest = new UserResource(source.Id);
            return Map(source, dest);
        }

        public UserResource Map(User source, UserResource dest)
        {
            dest.Id = source.Id;
            dest.Name = source.Name;
            dest.Email = source.Email;
            dest.Picture = source.Picture;
            dest.Role = source.Role;
            dest.NickName = source.NickName;
            return dest;
        }
    }
}