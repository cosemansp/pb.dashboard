﻿using System;
using PB.Dashboard.Api.Resources;
using PB.Dashboard.Core;
using PB.Dashboard.Domain.Entities;
using Raven.Client;

namespace PB.Dashboard.Api.Mappers
{
    public class Measurement2MeasurementResourceMapper : IMapper<Measurement, MeasurementResource>
    {
        private readonly IDocumentSession _session;

        public Measurement2MeasurementResourceMapper(IDocumentSession session)
        {
            _session = session;
        }

        public MeasurementResource Map(Measurement source)
        {
            var dest = new MeasurementResource();
            return Map(source, dest);
        }

        public MeasurementResource Map(Measurement source, MeasurementResource dest)
        {
            // date
            var date = source.Date;
            //if (source.Date.Kind == DateTimeKind.Unspecified)
            //    date = new DateTime(source.Date.Ticks, DateTimeKind.Local);

            // user
            var who = source.Who;
            if (!string.IsNullOrEmpty(source.UserId))
            {
                var user = _session.Load<User>(source.UserId);
                who = user.Name;
            }

            dest.Id = source.Id;
            dest.Zone = source.Zone;
            dest.Place = source.Place;
            dest.Date = source.Date.ToString("dd/MM/yy");
            dest.Time = source.Date.ToString("HH:mm");
            dest.DateTime = date;
            dest.Who = who;
            dest.Viz3m = source.GetSample(3).Viz;
            dest.Temp3m = source.GetSample(3).Temp;
            dest.Viz6m = source.GetSample(6).Viz;
            dest.Temp6m = source.GetSample(6).Temp;
            dest.Viz9m = source.GetSample(9).Viz;
            dest.Temp9m = source.GetSample(9).Temp;
            dest.Viz12m = source.GetSample(12).Viz;
            dest.Temp12m = source.GetSample(12).Temp;
            dest.Viz15m = source.GetSample(15).Viz;
            dest.Temp15m = source.GetSample(15).Temp;
            dest.TempAir = source.TempAir;
            return dest;
        }
    }
}