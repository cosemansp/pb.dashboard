﻿namespace PB.Dashboard.Api.Resources
{
    public class StatsResource
    {
        public int Measurements { get; set; }
        public int SpeciesInventories { get; set; }
        public int Users { get; set; }
    }
}