﻿using System;

namespace PB.Dashboard.Api.Resources
{
    public class MeasurementResource
    {
        // ReSharper disable InconsistentNaming
        public int Id { get; set; }
        public DateTime DateTime { get; set; }
        public string Place { get; set; }
        public string Zone { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Who { get; set; }
        public float Viz3m { get; set; }
        public float Viz6m { get; set; }
        public float Viz9m { get; set; }
        public float Viz12m { get; set; }
        public float Viz15m { get; set; }
        public float Temp3m { get; set; }
        public float Temp6m { get; set; }
        public float Temp9m { get; set; }
        public float Temp12m { get; set; }
        public float Temp15m { get; set; }
        public float TempAir { get; set; }
        // ReSharper restore InconsistentNaming 
    }
}