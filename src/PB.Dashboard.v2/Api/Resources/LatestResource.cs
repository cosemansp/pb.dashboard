﻿using System;

namespace PB.Dashboard.Api.Resources
{
    public class LatestResource
    {
        public Int64 Date { get; set; }
        public string DateString { get; set; }
        public double TempAve { get; set; }
        public double TempMin { get; set; }
        public double TempMax { get; set; }
        public double VizAve { get; set; }
        public double VizMin { get; set; }
        public double VizMax { get; set; } 
    }
}