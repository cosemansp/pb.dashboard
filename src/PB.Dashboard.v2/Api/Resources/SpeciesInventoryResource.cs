﻿using System;
using System.Collections.Generic;

namespace PB.Dashboard.Api.Resources
{
    public class ObservationResource
    {
        public string Specie { get; set; }
        public string Number { get; set; }
        public string Size { get; set; }
    }

    public class SpeciesInventoryResource
    {
        // ReSharper disable InconsistentNaming
        public int Id { get; set; }
        public DateTime DateTime { get; set; }
        public string Location { get; set; }
        public string UserName { get; set; }
        public string UserId { get; set; }
        public float WaterTemp { get; set; }
        public int DiveTime { get; set; }
        public string Comments { get; set; }

        public List<ObservationResource> Observations{ get; set;} 
        // ReSharper restore InconsistentNaming 

        public SpeciesInventoryResource()
        {
            Observations = new List<ObservationResource>();
        }
    }
}