﻿namespace PB.Dashboard.Api.Resources
{
    public class UserResource
    {
        public UserResource(string id)
        {
            Id = id;
        }

        public string Name { get; set; }
        public string Email { get; set; }
        public string Id { get; set; }
        public string Role { get; set; }
        public string Picture { get; set; }
        public string NickName { get; set; }
    }
}