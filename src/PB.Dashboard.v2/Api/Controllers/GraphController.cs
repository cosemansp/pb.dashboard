﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using PB.Dashboard.Core;
using PB.Dashboard.Domain.Entities;
using Raven.Client;
using Raven.Client.Linq;

namespace PB.Dashboard.Api.Controllers
{
    public class PlotEntryEx : List<object>
    {
        public PlotEntryEx(DateTime ts, double value1, double value2)
        {
            Add(ts.ToJsonDate());
            Add(SafeDouble(value1));
            Add(SafeDouble(value2));
        }

        public PlotEntryEx(DateTime ts, double value1)
        {
            Add(ts.ToJsonDate());
            Add(SafeDouble(value1));
        }

        private double? SafeDouble(double value)
        {
            if (value > -99.0)
                return value;
            return null;
        }
    }

    public class Serie
    {
        // ReSharper disable InconsistentNaming
        public string name { get; set; }
        public List<PlotEntryEx> data { get; set; }
        public string dashStyle { get; set; }
        public int zIndex { get; set; }
        public int lineWidth { get; set; }
        public string type { get; set; }
        public double fillOpacity { get; set; }
        // ReSharper restore InconsistentNaming
        public Serie()
        {
            dashStyle = "solid";
            zIndex = 1;
            lineWidth = 1;
            fillOpacity = 1.0;
        }
    }

    public class GraphController : ApiController
    {
        private readonly IDocumentSession _session;

        public GraphController(IDocumentSession session)
        {
            _session = session;
        }

        [Route("~/api/chartTemp")]
        public IHttpActionResult GetTemp(String regio)
        {
            var measurementList = _session.Query<Measurement>()
                                          .Where(x => x.Place == regio)
                                          .OrderBy(x => x.Date).ToList();

            var serieAir = new List<PlotEntryEx>();
            var serie3m = new List<PlotEntryEx>();
            var serie6m = new List<PlotEntryEx>();
            var serie9m = new List<PlotEntryEx>();
            var serie12m = new List<PlotEntryEx>();
            var serie15m = new List<PlotEntryEx>();

            foreach (var measurement in measurementList)
            {
                serieAir.Add(new PlotEntryEx(measurement.Date, measurement.TempAir));
                serie3m.Add(new PlotEntryEx(measurement.Date, measurement.GetSample(3).Temp));
                serie6m.Add(new PlotEntryEx(measurement.Date, measurement.GetSample(6).Temp));
                serie9m.Add(new PlotEntryEx(measurement.Date, measurement.GetSample(9).Temp));
                serie12m.Add(new PlotEntryEx(measurement.Date, measurement.GetSample(12).Temp));
                serie15m.Add(new PlotEntryEx(measurement.Date, measurement.GetSample(15).Temp));
            }

            var series = new List<Serie>();
            series.Add(new Serie { name = "air", data = serieAir, dashStyle = "dash" });
            series.Add(new Serie { name = "3m", data = serie3m });
            series.Add(new Serie { name = "6m", data = serie6m });
            series.Add(new Serie { name = "9m", data = serie9m });
            series.Add(new Serie { name = "12m", data = serie12m });
            series.Add(new Serie { name = "15m", data = serie15m });
            
            return Ok(series);
        }

        [Route("~/api/chartViz")]
        public IHttpActionResult GetViz(String regio)
        {
            var measurementList = _session.Query<Measurement>()
                                          .Where(x => x.Place == regio)
                                          .OrderBy(x => x.Date).ToList();

            var serie3m = new List<PlotEntryEx>();
            var serie6m = new List<PlotEntryEx>();
            var serie9m = new List<PlotEntryEx>();
            var serie12m = new List<PlotEntryEx>();
            var serie15m = new List<PlotEntryEx>();

            foreach (var measurement in measurementList)
            {
                serie3m.Add(new PlotEntryEx(measurement.Date, measurement.GetSample(3).Viz));
                serie6m.Add(new PlotEntryEx(measurement.Date, measurement.GetSample(6).Viz));
                serie9m.Add(new PlotEntryEx(measurement.Date, measurement.GetSample(9).Viz));
                serie12m.Add(new PlotEntryEx(measurement.Date, measurement.GetSample(12).Viz));
                serie15m.Add(new PlotEntryEx(measurement.Date, measurement.GetSample(15).Viz));
            }

            var series = new List<Serie>();
            series.Add(new Serie { name = "3m", data = serie3m });
            series.Add(new Serie { name = "6m", data = serie6m });
            series.Add(new Serie { name = "9m", data = serie9m });
            series.Add(new Serie { name = "12m", data = serie12m });
            series.Add(new Serie { name = "15m", data = serie15m });

            return Ok(series);
        }

        [Route("~/api/chartAverageViz")]
        public IHttpActionResult GetAverageViz(String regio)
        {
            var measurementList = _session.Query<Measurement>()
                                          .Where(x => x.Place == regio)
                                          .OrderBy(x => x.Date).ToList();

            var serieAverageViz = new List<PlotEntryEx>();
            var serieRange = new List<PlotEntryEx>();

            foreach (var measurement in measurementList)
            {
                double totalViz = 0;
                double maxViz = 0;
                double minViz = double.MaxValue;
                foreach (var sample in measurement.Samples)
                {
                    if (sample.Viz < -99)
                        continue;
                    totalViz += sample.Viz;
                    maxViz = Math.Max(maxViz, sample.Viz);
                    minViz = Math.Min(minViz, sample.Viz);
                }
                serieAverageViz.Add(new PlotEntryEx(measurement.Date, totalViz / 5));
                serieRange.Add(new PlotEntryEx(measurement.Date, minViz, maxViz));
            }

            var series = new List<Serie>();
            series.Add(new Serie { name = "Visibility", data = serieAverageViz });
            series.Add(new Serie { name = "Range", data = serieRange, lineWidth = 0, zIndex = 0, type = "arearange", fillOpacity = 0.2 });

            return Ok(series);
        }

        [Route("~/api/chartAverageTemp")]
        public IHttpActionResult GetAverageTemp(String regio)
        {
            var measurementList = _session.Query<Measurement>()
                                          .Where(x => x.Place == regio)
                                          .OrderBy(x => x.Date).ToList();

            var serieAverageTemp = new List<PlotEntryEx>();
            var serieRange = new List<PlotEntryEx>();

            foreach (var measurement in measurementList)
            {
                double totalTemp = 0;
                double maxTemp = 0;
                double minTemp = double.MaxValue;
                foreach (var sample in measurement.Samples)
                {
                    if (sample.Temp < -99)
                        continue;
                    totalTemp += sample.Temp;
                    maxTemp = Math.Max(maxTemp, sample.Temp);
                    minTemp = Math.Min(minTemp, sample.Temp);
                }
                serieAverageTemp.Add(new PlotEntryEx(measurement.Date, totalTemp / 5));
                serieRange.Add(new PlotEntryEx(measurement.Date, minTemp, maxTemp));
            }

            var series = new List<Serie>();
            series.Add(new Serie { name = "Temperature", data = serieAverageTemp });
            series.Add(new Serie { name = "Range", data = serieRange, lineWidth = 0, zIndex = 0, type = "arearange", fillOpacity = 0.2 });

            return Ok(series);
        }
    }
}