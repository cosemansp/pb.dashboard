﻿using System.Linq;
using System.Web.Http;
using PB.Dashboard.Api.Resources;
using PB.Dashboard.Core;
using PB.Dashboard.Domain.Entities;
using PB.Dashboard.Domain.Services;
using Raven.Client;

namespace PB.Dashboard.Api.Controllers
{
    public class RegisterResource
    {
        public string Name { get; set; }
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
        public string UserId { get; set; }
        public string Picture { get; set; }
        public string Gender { get; set; }
    }

    [RoutePrefix("api/user")]
    public class UserController : ApiController
    {
        private readonly IDocumentSession _session;
        private readonly IMapper<User, UserResource> _mapper;
        private readonly IAuthService _authService;

        public UserController(IDocumentSession session, 
                              IMapper<User, UserResource> mapper,
                              IAuthService authService)
        {
            _session = session;
            _mapper = mapper;
            _authService = authService;
        }

        [Route("")]
        public IHttpActionResult Get(string filter = null)
        {
            var user = _authService.GetCurrentUser();
            if (!user.IsInRole("publisher"))
                return Unauthorized();

            var users = _session.Query<User>().OrderBy(x => x.Name).ToList();
            if (!string.IsNullOrEmpty(filter))
            {
                users = users.Where(x => x.Role.Contains(filter)).ToList();
            }
            var result = users.Select(x => _mapper.Map(x)).ToList();
            return Ok(result);
        }
    }
}