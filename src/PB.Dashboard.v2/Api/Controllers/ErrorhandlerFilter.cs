﻿using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Web.Http.Filters;
using NLog;

namespace PB.Dashboard.Api.Controllers
{
    public class ExceptionLoggingAttribute : ExceptionFilterAttribute
    {
        private static readonly Logger NLog = LogManager.GetLogger("Exception");

        public override void OnException(HttpActionExecutedContext context)
        {
            //Log Critical errors
            NLog.LogException(LogLevel.Error, RequestToString(context.Request), context.Exception);
        }

        private static string RequestToString(HttpRequestMessage request)
        {
            var message = new StringBuilder();
            if (request.Method != null)
                message.Append(request.Method);

            if (request.RequestUri != null)
                message.Append(" ").Append(request.RequestUri);

            return message.ToString();
        }
    }
}