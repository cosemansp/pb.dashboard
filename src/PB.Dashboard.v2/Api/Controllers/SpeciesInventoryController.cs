﻿using System;
using System.Linq;
using System.Web.Http;
using Newtonsoft.Json;
using NLog;
using PB.Dashboard.Api.Commands;
using PB.Dashboard.Api.Resources;
using PB.Dashboard.Core;
using PB.Dashboard.Domain.Entities;
using PB.Dashboard.Domain.Services;
using Raven.Abstractions.Commands;
using Raven.Client;

namespace PB.Dashboard.Api.Controllers
{
    [ExceptionLogging]
    [RoutePrefix("api/speciesInventory")]
    public class SpeciesInventoryController : ApiController
    {
        private readonly IDocumentSession _session;
        private readonly IMapper<SpeciesInventory, SpeciesInventoryResource> _mapper;
        private readonly IAuthService _authService;
        private readonly Logger _log = LogManager.GetLogger("Exception");

        public SpeciesInventoryController(IDocumentSession session,
                                          IMapper<SpeciesInventory, SpeciesInventoryResource> mapper,
                                          IAuthService authService)
        {
            _session = session;
            _mapper = mapper;
            _authService = authService;
        }

        [Route("")]
        public IHttpActionResult Get()
        {
            var itemList = _session.Query<SpeciesInventory>()
                                   .Include<SpeciesInventory>(x => x.UserId)
                                   .Customize(x => x.WaitForNonStaleResults(TimeSpan.FromSeconds(3)))
                                   .ToList();

            var resources = itemList.Select(x => _mapper.Map(x)).ToList();
            return Ok(resources);
        }

        [Route("{id:int}", Name = "SpeciesInventoryGetById")]
        public IHttpActionResult Get(int id)
        {
            var entity = _session.Load<SpeciesInventory>(id);
            if (entity == null)
                return NotFound();

            return Ok(_mapper.Map(entity));
        }

        //[Authorize]
        [Route("")]
        public IHttpActionResult Post(SpeciesInventoryResource resource)
        {
            _log.Info("Post: {0}", JsonConvert.SerializeObject(resource));
            var user = _authService.GetCurrentUser();
            if (!user.IsInRole("user"))
                return Unauthorized();

            _log.Info("  User: {0}", user.Name);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            // create measurement
            var command = new CreateSpeciesInventoryCommand(_session, _authService);
            var entity = command.Execute(resource);
            _session.SaveChanges();

            // map and return
            var createdResource = _mapper.Map(entity);
            return Created(Url.Link("SpeciesInventoryGetById", new { id = createdResource.Id }), createdResource);
        }

        //[Authorize]
        [Route("{id:int}")]
        public IHttpActionResult Put(int id, SpeciesInventoryResource resource)
        {
            _log.Info("Put: {0}", JsonConvert.SerializeObject(resource));

            var user = _authService.GetCurrentUser();
            if (!user.IsInRole("user"))
                return Unauthorized();

            _log.Info("  User: {0}", user.Name);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            resource.Id = id;
            var command = new UpdateSpeciesInventoryCommand(_session, _authService);
            var entity = command.Execute(resource);
            if (entity == null)
                return NotFound();
            _session.SaveChanges();

            // map and return
            var updatedResource = _mapper.Map(entity);
            return Ok(updatedResource);
        }


        [Route("{id:int}")]
        public IHttpActionResult Delete(int id)
        {
            _log.Info("Delete: {0}", id);

            var user = _authService.GetCurrentUser();
            if (!user.IsInRole("user"))
                return Unauthorized();

            _log.Info("  User: {0}", user.Name);

            var entity = _session.Load<SpeciesInventory>(id);
            if (entity == null)
                return NotFound();

            _session.Advanced.Defer(new DeleteCommandData { Key = "SpeciesInventories/" + id });
            _session.SaveChanges();

            return Ok(_mapper.Map(entity));
        }

    }
}