﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using PB.Dashboard.Api.Resources;
using PB.Dashboard.Core;
using PB.Dashboard.Domain.Entities;
using Raven.Client;
using Raven.Client.Linq;

namespace PB.Dashboard.Api.Controllers
{
    public class ExportController : ApiController
    {
        private readonly IDocumentSession _session;

        public ExportController(IDocumentSession session)
        {
            _session = session;
        }

        [Route("~/api/export")]
        public HttpResponseMessage GetCsv()
        {
            var measurementList = _session.Query<Measurement>().OrderBy(x => x.Date).ToList();
            var sb = new StringBuilder();
            sb.AppendFormat("sep=,{0}", Environment.NewLine);
            sb.AppendFormat("DateTime,User,Place,Zone,AirTemp,Dept1,Viz1,Temp1,Dept2,Viz2,Temp2,Dept3,Viz3,Temp3,Dept4,Viz4,Temp4,Dept5,Viz5,Temp5{0}", Environment.NewLine);
            foreach (var item in measurementList)
            {
                sb.AppendFormat("{0:dd/MMM/yy H:mm},", item.Date.ToLocalTime());
                sb.AppendFormat("{0},", item.Who);
                sb.AppendFormat("{0},", item.Place);
                sb.AppendFormat("{0},", item.Zone);
                sb.AppendFormat(new CultureInfo("en-US"), "{0:0.0},", item.TempAir);
                foreach (var sample in item.Samples)
                {
                    sb.AppendFormat("{0},", sample.Dept);
                    sb.AppendFormat(new CultureInfo("en-US"), "{0:0.0},", sample.Viz);
                    sb.AppendFormat(new CultureInfo("en-US"), "{0:0.0},", sample.Temp);
                }
                sb.AppendFormat("{0}", Environment.NewLine);
            }

            var response = Request.CreateResponse(HttpStatusCode.OK, sb.ToString(), "text/csv");
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "export.csv"
            };
            return response;
        }

        [Route("~/api/latest")]
        public IHttpActionResult GetLatest()
        {
            var measurementList = _session.Query<Measurement>().OrderByDescending(x => x.Date).Take(1).ToList();
            var measurement = measurementList[0];
            double totalTemp = 0, maxTemp = 0;
            double minTemp = double.MaxValue;
            double totalViz = 0, maxViz = 0;
            double minViz = double.MaxValue;
            int cntTemp = 0, cntViz = 0;
            foreach (var sample in measurement.Samples)
            {
                if (sample.Temp > -99)
                {
                    totalTemp += sample.Temp;
                    maxTemp = Math.Max(maxTemp, sample.Temp);
                    minTemp = Math.Min(minTemp, sample.Temp);
                    cntTemp++;
                }
                if (sample.Viz > -99)
                {
                    totalViz += sample.Viz;
                    maxViz = Math.Max(maxViz, sample.Viz);
                    minViz = Math.Min(minViz, sample.Viz);
                    cntViz++;
                }
            }

            var resource = new LatestResource();
            resource.Date = measurement.Date.ToJsonDate();
            resource.DateString = measurement.Date.ToString("dd-MM-yyyy");
            resource.TempAve = totalTemp / cntTemp;
            resource.TempMin = minTemp;
            resource.TempMax = maxTemp;
            resource.VizAve = totalViz / cntViz;
            resource.VizMax = maxViz;
            resource.VizMin = minViz;

            return Ok(resource);
        }
    }
}