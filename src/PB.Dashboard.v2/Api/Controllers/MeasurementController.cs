﻿using System;
using System.Linq;
using System.Web.Http;
using PB.Dashboard.Api.Commands;
using PB.Dashboard.Api.Resources;
using PB.Dashboard.Core;
using PB.Dashboard.Domain.Entities;
using PB.Dashboard.Domain.Services;
using Raven.Abstractions.Commands;
using Raven.Client;
using Raven.Client.Linq;

namespace PB.Dashboard.Api.Controllers
{
    [RoutePrefix("api/measurement")]
    public class MeasurementController : ApiController
    {
        private readonly IDocumentSession _session;
        private readonly IMapper<Measurement, MeasurementResource> _mapper;
        private readonly IAuthService _authService;

        public MeasurementController(IDocumentSession session, 
                                     IMapper<Measurement, MeasurementResource> mapper,
                                     IAuthService authService)
        {
            _session = session;
            _mapper = mapper;
            _authService = authService;
        }

        [Route("")]
        public IHttpActionResult Get()
        {
            //var user = _authService.GetCurrentUser();
            //if (!user.IsInRole("user"))
            //    return Unauthorized();

            //throw new InvalidCastException("just an error");

            var measurementList = _session.Query<Measurement>()
                                          .Customize(x => x.WaitForNonStaleResults(TimeSpan.FromSeconds(3)))
                                          .OrderBy(x => x.Date)
                                          .Include(x => x.UserId).ToList();

            var resources = measurementList.Select(x => _mapper.Map(x)).ToList();
            return Ok(resources);
        }

        [Route("{id:int}", Name = "GetById")]
        public IHttpActionResult Get(int id)
        {
            var entity = _session.Load<Measurement>(id);
            if (entity == null)
                return NotFound();

            return Ok(_mapper.Map(entity));
        }

        //[Authorize]
        [Route("")]
        public IHttpActionResult Post(MeasurementResource resource)
        {
            var user = _authService.GetCurrentUser();
            if (!user.IsInRole("publisher"))
                return Unauthorized();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            // create measurement
            var command = new CreateMeasurementCommand(_session);
            var entity = command.Execute(resource);
            _session.SaveChanges();

            // map and return
            var createdResource = _mapper.Map(entity);
            return Created(Url.Link("GetById", new { id = createdResource.Id }), createdResource);
        }

        //[Authorize]
        [Route("{id:int}")]
        public IHttpActionResult Put(int id, MeasurementResource resource)
        {
            var user = _authService.GetCurrentUser();
            if (!user.IsInRole("publisher"))
                return Unauthorized();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            resource.Id = id;
            var command = new UpdateMeasurementCommand(_session);
            var entity = command.Execute(resource);
            if (entity == null)
                return NotFound();
            _session.SaveChanges();

            // map and return
            var updatedResource = _mapper.Map(entity);
            return Ok(updatedResource);
        }


        [Route("{id:int}")]
        public IHttpActionResult Delete(int id)
        {
            var user = _authService.GetCurrentUser();
            if (!user.IsInRole("admin"))
                return Unauthorized();

            var entity = _session.Load<Measurement>(id);
            if (entity == null)
                return NotFound();

            _session.Advanced.Defer(new DeleteCommandData { Key = "measurements/" + id });
            _session.SaveChanges();

            return Ok(_mapper.Map(entity));
        }

    }
}