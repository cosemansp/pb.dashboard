﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using PB.Dashboard.Api.Resources;
using PB.Dashboard.Core;
using PB.Dashboard.Domain.Entities;
using Raven.Client;
using Raven.Client.Linq;

namespace PB.Dashboard.Api.Controllers
{
    public class StatsController : ApiController
    {
        private readonly IDocumentSession _session;

        public StatsController(IDocumentSession session)
        {
            _session = session;
        }

        [Route("~/api/stats")]
        public IHttpActionResult Get()
        {
            var measurements = _session.Query<Measurement>().Count();
            var users = _session.Query<User>().Count();
            var fishInventories = _session.Query<SpeciesInventory>().Count();
            var response = new StatsResource
            {
                Measurements = measurements,
                Users = users,
                SpeciesInventories = fishInventories
            };
            return Ok(response);
        }

    }
}