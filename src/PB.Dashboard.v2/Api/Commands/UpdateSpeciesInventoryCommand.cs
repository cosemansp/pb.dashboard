﻿using System;
using System.Collections.Generic;
using System.Globalization;
using PB.Dashboard.Api.Resources;
using PB.Dashboard.Core;
using PB.Dashboard.Domain.Entities;
using PB.Dashboard.Domain.Services;
using Raven.Client;

namespace PB.Dashboard.Api.Commands
{
    public class UpdateSpeciesInventoryCommand : IModelCommand<SpeciesInventoryResource, SpeciesInventory>
    {
        private readonly IDocumentSession _session;
        private readonly IAuthService _authService;

        public UpdateSpeciesInventoryCommand(IDocumentSession session, IAuthService authService)
        {
            _session = session;
            _authService = authService;
        }

        public SpeciesInventory Execute(SpeciesInventoryResource model)
        {
            var entity = _session.Load<SpeciesInventory>(model.Id);
            if (entity == null)
                return null;

            //var user = _authService.GetCurrentUser();

            entity.DateTime = model.DateTime;
            entity.DiveTime = model.DiveTime;
            entity.WaterTemp = model.WaterTemp;
            entity.Location = model.Location;
            entity.Comments = model.Comments;
            entity.Observations = new List<Observation>();
            foreach (var observation in model.Observations)
            {
                // don't add empty species
                if (observation.Number == "0" || string.IsNullOrEmpty(observation.Specie))
                    continue;

                entity.Observations.Add(new Observation
                {
                    Specie = (Specie)Enum.Parse(typeof(Specie), observation.Specie),
                    Number = observation.Number,
                    Size = observation.Size
                });
            }
            _session.Store(entity);
            return entity;
        }
    }
}