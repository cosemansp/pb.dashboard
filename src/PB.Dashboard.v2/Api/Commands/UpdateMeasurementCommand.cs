﻿using System;
using System.Globalization;
using PB.Dashboard.Api.Resources;
using PB.Dashboard.Core;
using PB.Dashboard.Domain.Entities;
using Raven.Client;

namespace PB.Dashboard.Api.Commands
{
    public class UpdateMeasurementCommand : IModelCommand<MeasurementResource, Measurement>
    {
        private readonly IDocumentSession _session;

        public UpdateMeasurementCommand(IDocumentSession session)
        {
            _session = session;
        }

        public Measurement Execute(MeasurementResource model)
        {
            var entity = _session.Load<Measurement>(model.Id);
            if (entity == null)
                return null;

            entity.Date = model.DateTime;
            entity.Place = model.Place;
            entity.Zone = model.Zone;
            entity.Who = model.Who;
            var sample3M = entity.GetSample(3);
            sample3M.Temp = model.Temp3m.RoundTo(0.5f);
            sample3M.Viz = model.Viz3m.RoundTo(0.5f);
            var sample6M = entity.GetSample(6);
            sample6M.Temp = model.Temp6m.RoundTo(0.5f);
            sample6M.Viz = model.Viz6m.RoundTo(0.5f);
            var sample9M = entity.GetSample(9);
            sample9M.Temp = model.Temp9m.RoundTo(0.5f);
            sample9M.Viz = model.Viz9m.RoundTo(0.5f);
            var sample12M = entity.GetSample(12);
            sample12M.Temp = model.Temp12m.RoundTo(0.5f);
            sample12M.Viz = model.Viz12m.RoundTo(0.5f);
            var sample15M = entity.GetSample(15);
            sample15M.Temp = model.Temp15m.RoundTo(0.5f);
            sample15M.Viz = model.Viz15m.RoundTo(0.5f);
            entity.TempAir = model.TempAir.RoundTo(0.5f);

            _session.Store(entity);
            return entity;
        }
    }
}