﻿using System;
using System.Globalization;
using System.Net.Http;
using PB.Dashboard.Api.Resources;
using PB.Dashboard.Core;
using PB.Dashboard.Domain.Entities;
using Raven.Client;

namespace PB.Dashboard.Api.Commands
{
    public class CreateMeasurementCommand : IModelCommand<MeasurementResource, Measurement>
    {
        private readonly IDocumentSession _session;

        public CreateMeasurementCommand(IDocumentSession session)
        {
            _session = session;
        }

        public Measurement Execute(MeasurementResource model)
        {
            // create and fill measurement
            var measurement = new Measurement { Place = model.Place, Zone = model.Zone, Date = model.DateTime };
            if (string.IsNullOrEmpty(measurement.Place))
                measurement.Place = "Muisbroek";
            if (string.IsNullOrEmpty(measurement.Zone))
                measurement.Zone = "Grote Boei";
            measurement.Who = model.Who;
            measurement.Samples.Add(new Sample { Dept = 3, Temp = RoundTo(model.Temp3m, 0.5f), Viz = RoundTo(model.Viz3m, 0.5f) });
            measurement.Samples.Add(new Sample { Dept = 6, Temp = RoundTo(model.Temp6m, 0.5f), Viz = RoundTo(model.Viz6m, 0.5f) });
            measurement.Samples.Add(new Sample { Dept = 9, Temp = RoundTo(model.Temp9m, 0.5f), Viz = RoundTo(model.Viz9m, 0.5f) });
            measurement.Samples.Add(new Sample { Dept = 12, Temp = RoundTo(model.Temp12m, 0.5f), Viz = RoundTo(model.Viz12m, 0.5f) });
            measurement.Samples.Add(new Sample { Dept = 15, Temp = RoundTo(model.Temp15m, 0.5f), Viz = RoundTo(model.Viz15m, 0.5f) });
            measurement.TempAir = RoundTo(model.TempAir, 0.5f);

            // store in db
            _session.Store(measurement);
            return measurement;
        }

        private float RoundTo(float value, float roundto)
        {
            if (Math.Abs(roundto) <= 0)
            {
                return value;
            }
            return (float)Math.Floor(value / roundto) * roundto;
        }
    }
}