﻿using System;
using System.Globalization;
using System.Net.Http;
using PB.Dashboard.Api.Resources;
using PB.Dashboard.Core;
using PB.Dashboard.Domain.Entities;
using PB.Dashboard.Domain.Services;
using Raven.Client;

namespace PB.Dashboard.Api.Commands
{
    public class CreateSpeciesInventoryCommand : IModelCommand<SpeciesInventoryResource, SpeciesInventory>
    {
        private readonly IDocumentSession _session;
        private readonly IAuthService _authService;

        public CreateSpeciesInventoryCommand(IDocumentSession session, IAuthService authService)
        {
            _session = session;
            _authService = authService;
        }

        public SpeciesInventory Execute(SpeciesInventoryResource model)
        {
            var user = _authService.GetCurrentUser();

            // create and fill measurement
            var entity = new SpeciesInventory { 
                Location = model.Location, 
                DateTime = model.DateTime, 
                DiveTime = model.DiveTime,
                WaterTemp = model.WaterTemp,
                Comments = model.Comments,
                UserId = (user != null) ? user.Id : null
            };

            foreach (var observation in model.Observations)
            {
                // don't add empty species
                if (observation.Number == "0" || string.IsNullOrEmpty(observation.Specie) )
                    continue;

                entity.Observations.Add(new Observation
                {
                    Specie = (Specie)Enum.Parse(typeof(Specie), observation.Specie),
                    Number = observation.Number,
                    Size = observation.Size
                });
            }

            // store in db
            _session.Store(entity);
            return entity;
        }

        private float RoundTo(float value, float roundto)
        {
            if (Math.Abs(roundto) <= 0)
            {
                return value;
            }
            return (float)Math.Floor(value / roundto) * roundto;
        }
    }
}