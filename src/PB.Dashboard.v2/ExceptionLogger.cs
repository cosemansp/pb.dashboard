﻿using System.Net.Http;
using System.Text;
using System.Web.Http.ExceptionHandling;
using NLog;

namespace PB.Dashboard
{
    public class NLogExceptionLogger : ExceptionLogger
    {
        private static readonly Logger NLog = LogManager.GetLogger("Exception");
        public override void Log(ExceptionLoggerContext context)
        {
            NLog.LogException(LogLevel.Error, RequestToString(context.Request), context.Exception);
            NLog.Error(context.Exception.ToString());
        }

        private static string RequestToString(HttpRequestMessage request)
        {
            var message = new StringBuilder();
            if (request.Method != null)
                message.Append(request.Method);

            if (request.RequestUri != null)
                message.Append(" ").Append(request.RequestUri);

            return message.ToString();
        }
    }
}